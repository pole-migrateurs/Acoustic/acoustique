#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 11 14:23:14 2020

@author: gfernandezgarcia
"""

import cv2
import os
import numpy as np
from ctypes import *
import math
import random
import time
import darknet
import datetime
import copy

# To test without the DL architecture, comment line 'import darknet'

def folderOrganiser(pwd, vid_formats):
    """
    Function that organise the files. Starting from a folder with
    a list of files, it creates a folder per file and moves the video into it.

    Input:
    - path of the folder
    - list of accepted video formats, defined in main 
    """
    for r, d, f in os.walk(pwd):
        for file in f:
            for form in vid_formats:
                if file.endswith(form):
                    
                    folder = os.path.splitext(file)[0]
                    path = os.path.join(r,folder)
                    start = os.path.join(r,file)
                    final = os.path.join(path,file)
                
                    try:
                        os.mkdir(path)
                    except FileExistsError:
                        print('Folder', folder, ' exist already!')
                
                    try:
                        os.rename(start,final)
                    except FileNotFoundError:
                        if os.path.isfile(final):
                            print('I\'ve already found the following video file in its right place: I do nothing!')
                            print(final)
                        else:
                            print('Something wrong! I have not found the video neither in starting or final folder!')
                            print(start)
                            print(final)
    return

def bgkSegMain(pwd, vid_form):
    """
    Function that call the segmentation routine, feeding the parameters specific for the camera.

    Input:
    - path of the folder
    - list of accepted video formats, defined in main 

    Output:
    - list of paths of original videos
    - list of paths of filtered videos
    """
    
    videos = []

    for root, dirs, files in os.walk(pwd):
        for file in files:
            for form in vid_form:
                if file.endswith(form):
                    videos.append(os.path.join(root, file))
    
    for vid in videos:
        vid_name = os.path.basename(vid)
        camera = vid_name.split('_')[0]
        if camera == 'DIDSON':
            # threshold set to 130
            threshold = 130
            bgkSeg(vid, threshold)
        elif camera == 'ARIS':
            # threshold set to 50
            threshold = 10
            bgkSeg(vid, threshold)
        elif camera == 'OCULUS':
            print("The OCULUS camera is not yet implemented")
        elif camera == 'BLUEVIEW':
            print("The BLUEVIEW camera is not yet implemented")
        else:
            print("Something wrong! Check if the filenames respect the codename - I was unable to get the camera type.")
        
    # create array with FILT videos
    videos_filt = []
    for root, dirs, files in os.walk(pwd):
        for file in files:
            if file.endswith('_FILT.mp4'):
                videos_filt.append(os.path.join(root, file))
    return videos, videos_filt

def bgkSeg(video, t):
    """
    Function that segmentates a video.

    Input:
    - path of a video
    - threshold parameter

    Ouput:
    - filtered video
    """
    
    cap = cv2.VideoCapture(video)
    fgbg = cv2.createBackgroundSubtractorMOG2(varThreshold = t)
    x = int(cap.get(3))
    y = int(cap.get(4))
    fps = int(cap.get(5))

    root, video_name = os.path.split(video)
    parse = video_name.split('.')
    video_name = parse[0] + '_FILT.mp4'
    filename = os.path.join(root, video_name)

    if (cap.isOpened()== True):
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        out = cv2.VideoWriter(filename,fourcc, fps, (x,y),0)

        while(1):
          ret, frame = cap.read()
          if ret == True:
             fgmask = fgbg.apply(frame)
             out.write(fgmask)
          else:
             break

        cap.release()
        out.release()
        cv2.destroyAllWindows()
    else:
        print("Error with video stream for", video)
    return

def denoiser(listOfVids):
    """
    This function does 2 operations:
    1) it denoise a list of filtered videos
    2) it compose the final videos, in which the images BGR are, in order,
    B = original image
    G = filtered image
    R = denoised image

    Input:
    - A list of paths of videos. For n videos, such list has the dimension 2 x n (it is a list of lists)
    since for each video the function needs also the path of the filtered video

    Output:
    - A list of paths of videos ready to be feed into the DL architecture
    """
    
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    
    listForDL = []

    print('Starting Denoiser')

    for i in range(len(listOfVids)):
        cap_orig = cv2.VideoCapture(listOfVids[i][0])
        cap_filt = cv2.VideoCapture(listOfVids[i][1])
        print('Starting denoising on video:', listOfVids[i][0])
        img_array = []

        while True:

            ret_o, frame_o = cap_orig.read()
            ret_f, frame_f = cap_filt.read()

            if ret_o and ret_f:
               h, w, c = frame_f.shape
               size = (w,h)
               dst = cv2.medianBlur(frame_f,3)

               opening = cv2.morphologyEx(dst, cv2.MORPH_OPEN, kernel)

               img_multi = np.zeros((h, w, 3), dtype=np.uint8)
                         
               img_multi[:,:,0] = frame_o[:,:,0]
               img_multi[:,:,1] = frame_f[:,:,1]
               img_multi[:,:,2] = opening[:,:,2]

               img_multi = drawAxis(img_multi)

               img_array.append(img_multi)

            elif ret_o is False:
               print('I have a problem in opening the video: check the path of the video:' , listOfVids[i][0])
               break
            elif ret_f is False:
               print('I have a problem in opening the video: check the path of the video:' , listOfVids[i][1])
               break

        vid_name = listOfVids[i][1].replace('FILT','obd')
        fps = int(cap_orig.get(5))
        out = cv2.VideoWriter(vid_name,cv2.VideoWriter_fourcc(*'DIVX'), fps, size)
        for f in range(len(img_array)):
            out.write(img_array[f])
        out.release()
        listForDL.append(vid_name)

    return listForDL


######################################
# BEGINNING DEEP LEARNING ARCHITECTURE
######################################

def drawAxis(img):
    """
    This function draws a green axis in pixels on the final images.
    This is done to rely the bounding boxes coordinates to the image.

    Input:
    - an image

    Output:
    - the same image with axis
    """
    ny, nx, nz = img.shape
    start = (0,ny)
    end_x = (nx,ny)
    end_y = (0,0)

    cv2.line(img,start,end_x,(0,255,0),5)
    cv2.line(img,start,end_y,(0,255,0),5)

    lenght=100
    tot_xticks = int(nx/lenght)
    tot_yticks = int(ny/lenght)

    for i in range(tot_xticks+1):
        tick_x_start = (lenght*i,ny)
        tick_x_end = (lenght*i,ny-10)
        cv2.line(img,tick_x_start,tick_x_end,(0,255,0),5)
        text_pos = (lenght*i-15,ny-15)
        cv2.putText(img,str(lenght*i),text_pos,cv2.FONT_HERSHEY_SIMPLEX,0.5,(0,255,0),2)

    for i in range(tot_yticks+1):
        tick_y_start = (0,lenght*i)
        tick_y_end = (10,lenght*i)
        cv2.line(img,tick_y_start,tick_y_end,(0,255,0),5)
        text_pos = (15,lenght*i)
        cv2.putText(img,str(lenght*i),text_pos,cv2.FONT_HERSHEY_SIMPLEX,0.5,(0,255,0),2)

    return img

# Starting YOLO functions

def convertBack(x, y, w, h):
    """
    Function that converts the bounding boxes from (x_c, y_c, h, w) to (xmin, ymin, xmax, ymax).
    where:
    x_c = x coordinate of the center of the box
    y_c = y coordinate of the center of the box
    h = height of the box
    w = width of the box

    NOTE: all values are in relative values, as fractions of the image dimension

    Input:
    - (x_c, y_c, h, w) of the bounding box
  
    Output:
    - (xmin, ymin, xmax, ymax) of the bounding box
    """
    xmin = int(round(x - (w / 2)))
    xmax = int(round(x + (w / 2)))
    ymin = int(round(y - (h / 2)))
    ymax = int(round(y + (h / 2)))
    return xmin, ymin, xmax, ymax

def cvDrawBoxes(detections, img):
    """
    Function that draws the bounding boxes

    Input:
    - a list of detections
    - image

    Output:
    - image with detections
    """
    for detection in detections:
        x, y, w, h = detection[2][0],\
            detection[2][1],\
            detection[2][2],\
            detection[2][3]
        xmin, ymin, xmax, ymax = convertBack(
            float(x), float(y), float(w), float(h))
        pt1 = (xmin, ymin)
        pt2 = (xmax, ymax)
        cv2.rectangle(img, pt1, pt2, (0, 255, 0), 1)
        cv2.putText(img,
                    detection[0].decode() +
                    " [" + str(round(detection[1] * 100, 2)) + "]",
                    (pt1[0], pt1[1] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                    [0, 255, 0], 2)
    return img

netMain = None
metaMain = None
altNames = None

def YOLO(cfg,weight,meta,video):
    """
    Function that calls and loads Yolo v3. It calls also the forward and backward filters.
    The function prints also the detections data in the stdout

    Input:
    - a config file
    - a weight file
    - a metadata file
    - the path of a video

    Output:
    - the function saves the final videos
    """

    global metaMain, netMain, altNames
    configPath = cfg
    weightPath = weight
    metaPath = meta
    if not os.path.exists(configPath):
        raise ValueError("Invalid config path `" +
                         os.path.abspath(configPath)+"`")
    if not os.path.exists(weightPath):
        raise ValueError("Invalid weight path `" +
                         os.path.abspath(weightPath)+"`")
    if not os.path.exists(metaPath):
        raise ValueError("Invalid data file path `" +
                         os.path.abspath(metaPath)+"`")
    if netMain is None:
        netMain = darknet.load_net_custom(configPath.encode(
            "ascii"), weightPath.encode("ascii"), 0, 1)  # batch size = 1
    if metaMain is None:
        metaMain = darknet.load_meta(metaPath.encode("ascii"))
    if altNames is None:
        try:
            with open(metaPath) as metaFH:
                metaContents = metaFH.read()
                import re
                match = re.search("names *= *(.*)$", metaContents,
                                  re.IGNORECASE | re.MULTILINE)
                if match:
                    result = match.group(1)
                else:
                    result = None
                try:
                    if os.path.exists(result):
                        with open(result) as namesFH:
                            namesList = namesFH.read().strip().split("\n")
                            altNames = [x.strip() for x in namesList]
                except TypeError:
                    pass
        except Exception:
            pass
    cap = cv2.VideoCapture(video)
    w = int(cap.get(3))
    h = int(cap.get(4))
    cap.set(3, 1280)
    cap.set(4, 720)
    out_name = video.replace('obd','DL')
    print('Looking at videos:', out_name)
    years = int(os.path.basename(out_name).split('_')[1].split('-')[0])
    months = int(os.path.basename(out_name).split('_')[1].split('-')[1])
    days = int(os.path.basename(out_name).split('_')[1].split('-')[2])
    plain_time = os.path.basename(out_name).split('_')[2]
    hours = int(plain_time[0:2])
    minutes = int(plain_time[2:4])
    seconds = int(plain_time[4:6])
    t = datetime.datetime(years, months, days, hours, minutes, seconds, 00)
    t = t - datetime.datetime(years, months, days, 0, 0, 0, 0)
    fps = int(cap.get(cv2.CAP_PROP_FPS))
    tot_frame = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    out = cv2.VideoWriter(out_name, cv2.VideoWriter_fourcc(*'DIVX'), fps,(w,h))
    print("Starting the YOLO loop...")
    print("(The coordinates of the center of the bounding boxes are given)")
    print("Timecode, Timestamp, FPS, Frame, Tot Frames, Prob %, Box: x, y, w, h, area, diagonal")

    # Create an image we reuse for each detect
    darknet_image = darknet.make_image(darknet.network_width(netMain),
                                    darknet.network_height(netMain),3)
    old_detections = []
    
    while True:
        prev_time = time.time()
        ret, frame_read = cap.read()
        if ret == True:
            net_w = darknet.network_width(netMain)
            net_h = darknet.network_height(netMain)
            detections, frame_resized = detector(frame_read,net_w,net_h,darknet_image)
            nx,ny,nz = frame_resized.shape
            r_x = nx/w
            r_y = ny/h
            detections = timefilter_forward(detections,cap,net_w,net_h,darknet_image)
            detections = timefilter_backward(detections,old_detections)
            old_detections = copy.deepcopy(detections)
            detections = printer(cap,t,fps,tot_frame,detections,r_x,r_y)
            writer(frame_resized,detections,w,h,out)
        else:
            break
    cap.release()
    out.release()

def detector(frame_r, net_w, net_h, base_image):
    """
    Function that calls Yolo v3 on a specific image of a video.

    Input:
    - frame to be analysed
    - network dimensions (net_w, net_h)
    - base image, necessary for darknet

    Output:
    - list of detections
    - frame resized to network dimensions
    """
    frame_rgb = cv2.cvtColor(frame_r, cv2.COLOR_BGR2RGB)
    frame_res = cv2.resize(frame_rgb,(darknet.network_width(netMain),darknet.network_height(netMain)),interpolation=cv2.INTER_LINEAR)
    darknet.copy_image_from_bytes(base_image,frame_res.tobytes())
    detects = darknet.detect_image(netMain, metaMain, base_image, thresh=0.25)
    return detects,frame_res

def timefilter_forward(detects,cap,net_w,net_h,base_image):
    """
    Forward filter. It checks the presence of coherent detection on the next frame.

    Input:
    - list of detections
    - counter of video analysis, it marks which frame is currently analised
    - network dimensions (net_w, net_h)
    - base image, necessary for darknet

    Ouput:
    - list of surviving detections
    """
    if len(detects) >= 1:
        current_frame = int(cap.get(cv2.CAP_PROP_POS_FRAMES))
        next_ret, next_frame_read = cap.read()
        if next_ret:
            next_detects, nextframe_resized = detector(next_frame_read,net_w,net_h,base_image)
            cap.set(cv2.CAP_PROP_POS_FRAMES, current_frame)
            if len(next_detects) >= 1:
                for detect in detects:
                    for next_detect in next_detects:
                        good_detect = OverlapFilter(detect,next_detect)
                        ind = detects.index(detect)
                        l_detect=list(detect)
                        if len(l_detect) == 3:
                            l_detect.append(good_detect)
                        else:
                            l_detect[-1] = good_detect
                        detect=tuple(l_detect)
                        detects[ind]=detect

            else:
                for detect in detects:
                    ind = detects.index(detect)
                    l_detect=list(detect)
                    l_detect.append(False)
                    detect=tuple(l_detect)
                    detects[ind]=detect

    return detects

def timefilter_backward(detects,old_detects):
    """
    Backward filter. It checks the presence of coherent detection on the frame before.

    Input:
    - list of detections
    - list of detections found in the old frame

    Output:
    - list of surviving detections
    """
    if len(detects) >= 1 and len(old_detects) >=1:
        for detect in detects:
            if detect[-1] is False:
                for old_detect in old_detects:
                    good_detect = OverlapFilter(detect,old_detect)
                    ind = detects.index(detect)
                    old_eval = detect[-1]
                    if good_detect:
                        l_detect=list(detect)
                        l_detect[-1] = good_detect
                        detect=tuple(l_detect)
                        detects[ind]=detect

    return detects

def OverlapFilter(det_1,det_2):
    """
    Overlap calculator. 

    NOTE: actually an overlap > 0 is enough

    Input:
    - two lists with the data of two single detections 

    Output:
    - a boolean true if the detections overlaps
    """
    x1, y1, w1, h1 = det_1[2][0], det_1[2][1], det_1[2][2], det_1[2][3]
    xmin1, ymin1, xmax1, ymax1 = convertBack(float(x1), float(y1), float(w1), float(h1))
    x2, y2, w2, h2 = det_2[2][0], det_2[2][1], det_2[2][2], det_2[2][3]
    xmin2, ymin2, xmax2, ymax2 = convertBack(float(x2), float(y2), float(w2), float(h2))
    x_l = max(xmin1,xmin2)
    x_r = min(xmax1,xmax2)
    y_t = max(ymin1,ymin2)
    y_b = min(ymax1,ymax2)
    if x_r < x_l or y_b < y_t:
        overlap = 0
    else:
        overlap = (abs(x_r - x_l)) * (abs(y_b - y_t))
    if overlap > 0 :
        good_det = True
    else:
        good_det = False

    return good_det

def printer(cap,t,fps,tot_frame,detects,rap_x,rap_y):
    """
    Function that prints on the stdout the data

    Input:
    - counter of video analysis, it marks which frame is currently analised
    - the datetime of the beginning of the video
    - fps
    - total number of frames in the video
    - list of surviving detections
    - ratios between network dimensions and real dimensions
    """
    if len(detects) >= 1:
        sec=int(cap.get(cv2.CAP_PROP_POS_MSEC))*0.001
        timestamp = datetime.timedelta(seconds=sec)
        num_frame = int(cap.get(cv2.CAP_PROP_POS_FRAMES))
        true_detects = []
        for detection in detects:
            if detection[-1]:
                true_detects.append(detection)
                area = (detection[2][2]/rap_x)*(detection[2][3]/rap_y)
                diag = np.sqrt(np.power(detection[2][2]/rap_x,2)+np.power(detection[2][3]/rap_y,2))
                print(t + timestamp, timestamp, fps, num_frame, tot_frame, f'{detection[1]*100:9.2f}',f'{detection[2][0]/rap_x:9.2f}',f'{detection[2][1]/rap_y:9.2f}',\
                        f'{detection[2][2]/rap_x:9.2f}',f'{detection[2][3]/rap_y:9.2f}', f'{area:9.2f}', f'{diag:9.2f}')
        detects = true_detects
    return detects

def writer(frame_res,detects,w,h,out):
    """
    Function that write the final images in the video container
    
    Input:
    - frame at network scale
    - list of detections
    - real image dimensions
    - video container
    """

    if len(detects) >= 1:
        image = cvDrawBoxes(detects, frame_res)
    else:
        image = frame_res
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image_resized = cv2.resize(image,(w,h),interpolation=cv2.INTER_LINEAR)
    out.write(image_resized)
    return

def main():
    # this variables are necessary only for YOLO
    cfg_path = "./cfg/yolov3_v1_obd.cfg"
    weight_path = "./backup/yolov3_v1_obd_best.weights"
    meta_path = "./cfg/voc_v1_obd.data"   

    # this is necessary for pretreatement
    vid_formats = ['.avi', '.mp4']

    home = 'path/of/the/videos'

    # pretreatement
    folderOrganiser(home, vid_formats)
    print('Done file organising')
    listOfOrig, listOfFilt = bgkSegMain(home, vid_formats)
    print('Done background segmentation')
    listOfVids = []
    for i in range(len(listOfOrig)):
        listOfVids.append([listOfOrig[i],listOfFilt[i]])
    print('Done array creation')
    listForYolo = denoiser(listOfVids)
    print('Done denoising')

    # calling YOLO
    for vid in listForYolo:
        YOLO(cfg_path,weight_path,meta_path,vid)
        
    return

if __name__ == "__main__":
    main()
