# Acoustique

This project is a C/Python pipeline to detect fish passages on DIDSON and ARIS cameras. It is based on modifications of the YOLOv3 CNN and data pretreatement. 
The projet is a fork of https://github.com/AlexeyAB/darknet. 

We made available also two datasets to test the pipeline:
- [Training Dataset (TD)](https://zenodo.org/record/5046708): composed by a set of annotated images
- [Validation Dataset (VD)](https://zenodo.org/record/5092010): composed by a real-case scenario of videos


## Requirements for Windows, Linux and macOS

### Requirements for compiling Darknet (taken from https://github.com/AlexeyAB/darknet):

- **CMake >= 3.18**
- **CUDA >= 10.2**
- **OpenCV >= 2.4**
- **cuDNN >= 8.0.2**
- **GPU with CC >= 3.0**

### Requirements for running the Python pipeline

A requirement.txt file is given in the main folder (see below). 

- **Python3**
- **scikit_image >= 0.16**
- **opencv_python >= 4.5.1**
- **numpy >= 1.13**

## How to compile

For an in-depth installation guide of the Darknet framework, please see: https://github.com/AlexeyAB/darknet

```
git clone 'https://forgemia.inra.fr/pole-migrateurs/acoustique'
cd darknet
make
```

Before compiling with `make`, you can set options in the `Makefile`. Concerning the installation of the Python3 packages we reccomend to use a virtual environment. This step is therefore optional. 

```
python3 -m venv /path/to/darknet/
source /path/to/darknet/bin/activate  (for linux systems)
\path\to\darknet\Scripts\activate.bat  (for Windows systems)
```

Install then the required packages:

`pip install -r requirements.txt`

If you are not using a virtual environment, verify that the version of the command `pip` is set to `pip3`.

## How to test 
### How to test inference on a single image

You need to check that in the data file `data/acoustique.data` the paths of `train`, `valid` and `names` variables point to the right files.

To test if the compilation of the Darknet framework has been successful just run:

`./darknet detector test data/acoustique.data cfg/yolov3_acoustique.cfg backup/yolov3_v1_obd_best.weights`

to run on Windows, replace `darknet` with `darknet.exe`. To test on an ARIS and on a DIDSON image, give their respective paths in the prompt command: `./testing/images/name_of_the_file.tiff`.

### How to test inference on an avi/mp4 video

To test the pipeline, change at line [591](https://forgemia.inra.fr/pole-migrateurs/acoustique/-/blob/master/pipeline_YOLOv3.py#L591) the `home` path to `./testing/videos/`. Then run simply: `python3 pipeline_YOLOv3.py > output.txt`

## How to run inference
### Data preparation

All the video files should be in the same folder. This path needs to be given at line [591](https://forgemia.inra.fr/pole-migrateurs/acoustique/-/blob/master/pipeline_YOLOv3.py#L591) in the `home` variable.

The filenames must follow the following standard:
`NAMEOFTHECAMERA_YYYY-MM-DD_hhmmss.videoformat`
where:
- `NAMEOFTHECAMERA = ARIS or DIDSON`
- `YYYY-MM-DD = date`
- `hhmmss = timecode`
- `videoformat = .mp4 or .avi`

for example:

**ARIS_2017-08-09_230000.mp4**

**DIDSON_2013-11-04_031500.avi**

### Run inference

You need to check that in the data file `data/acoustique.data` the paths of `train`, `valid` and `names` variables point to the right files. The paths of the `.cfg`, `.data` and `.weights` files must be given also in the `main` function of `pipeline_YOLOv3.py`, at lines [584](https://forgemia.inra.fr/pole-migrateurs/acoustique/-/blob/master/pipeline_YOLOv3.py#L584)-[586](https://forgemia.inra.fr/pole-migrateurs/acoustique/-/blob/master/pipeline_YOLOv3.py#L586)

Once the `home` variable is given and the files are renamed, the following command runs the model on all the files:
`python3 pipeline_YOLOv3.py > output.txt`



## Current limitations (solved in the v2)

- **The pretreatement is not parallelized**, only one video at the time is analysed.
- **It is mandatory to convert the video from .aris and .ddf format using the SoundMetrics software**.
- In the output **a line** is given for **each detection**.
